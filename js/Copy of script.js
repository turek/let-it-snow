var canvas = null,
	ctx = null,
	b_canvas = null,
	b_ctx = null,
	s_canvas = null,
	s_ctx = null,
	flakes = [],
	flake_timer = null,
	max_flakes = 0,
	max_radius = 10;

function init()
{
	canvas = document.getElementById("snow");
	ctx = canvas.getContext("2d");
	// resize canvas to match window size
	ctx.canvas.width  = window.innerWidth;
	//ctx.canvas.height = window.innerHeight;
	b_canvas = document.createElement("canvas");
	b_ctx = b_canvas.getContext("2d");
	b_ctx.canvas.width = ctx.canvas.width;
	b_ctx.canvas.height = ctx.canvas.height;
	s_canvas = document.getElementById("flake");
	s_ctx = s_canvas.getContext("2d");
	// set timer to add flakes every 200 ms
	flake_timer = setInterval(addFlake, 200);
	// draw scene
	Draw();
	//setInterval(animate, 30);
	// start animation 30 frames per second
	setInterval(function() {
		animate();
	}, 1000/30);
}

function animate()
{
	Update();
	Draw();
}

function Update()
{
	for (var i = 0; i < flakes.length; i++)
	{
		if (flakes[i].y < ctx.canvas.height)
		{
			flakes[i].y += flakes[i].speed;
			if (flakes[i].y >= ctx.canvas.height)
			{
				flakes[i].y = -5;
			}
			flakes[i].x += flakes[i].drift;
			if (flakes[i].x > ctx.canvas.width)
			{
				flakes[i].x = 0;
			}
		}
	}
}

function Draw()
{
	ctx.save();
	blank();
	for (var i = 0; i < flakes.length; i++) {
		b_ctx.putImageData(flakes[i].data, flakes[i].x, flakes[i].y);
	}
	// copy the entire rendered image from the buffer canvas to the visible one
	ctx.drawImage(b_canvas, 0, 0, b_canvas.width, b_canvas.height);
	ctx.restore();
}

function blank()
{
	b_ctx.clearRect(0, 0, b_ctx.canvas.width, b_ctx.canvas.height);
	b_ctx.fillStyle = "#330033";
	b_ctx.fillRect(0,0, b_ctx.canvas.width, b_ctx.canvas.height);
}

function addFlake() {
	$.ajax({
		url: 'get_flake.php',
		success: function( data )
		{
			var thiscoords = jQuery.parseJSON( data );
			flakes[flakes.length] = new Flake(flakes.length, 'sample title', s_ctx, thiscoords);
		}
	});
	// clear timer so no more flakes are added
	if (flakes.length >= max_flakes)
	{
		clearInterval(flake_timer);
	}
}


function Flake(id, text, ctx, coords)
{
	this.x = 0; //Math.round(Math.random() * ctx.canvas.width);
	this.y = 0;
	this.r = 0;
	this.drift = Math.random();
	this.speed = Math.round(Math.random() * 5) + 1;
	this.height = 250;
	//this.height = this.width;
	this.id = id;
	this.max_radial = max_radius;
	this.cut = coords;
	this.angle = 0;
	this.text = text;
	this.color = null;
	this.ctx = ctx;
	this.data = null;

	this.init = function()
	{
		width = this.ctx.canvas.width;
		height = this.ctx.canvas.height;
		this.x = (10 + Math.random() * (width - 2 * this.max_radial));
		this.y = -this.height;
		this.r = 0.2 * this.max_radial + Math.random() * (0.8 * this.max_radial);
		this.color = "rgba(255, 255, 255, " + .5 * ( height - this.max_radial ) / height + ")";

		this.ctx.save();
		this.ctx.translate(250, 250);
		this.draw();
		this.ctx.restore();

		this.data = this.ctx.getImageData(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
	}

	this.draw = function()
	{
		thislength = this.cut.length;
		thiscoords = this.cut;
		addx = 0;
		addy = 0;

		this.ctx.fillStyle = 'white';
		this.ctx.beginPath();
		this.ctx.arc(0, 0, 240, 0, Math.PI*2, false);
		this.ctx.fill();
		this.ctx.globalCompositeOperation = 'destination-out';

		this.ctx.fillStyle = 'black';
		for (var z = 1; z <= 4; z++) {
			this.ctx.beginPath();
			this.ctx.moveTo( thiscoords[0].x, thiscoords[0].y );
			for (var i = 1; i < thislength; i++ )
			{
				if (thiscoords[i].y == 'X')
				{
					if (typeof(thiscoords[i+1]) != 'undefined')
					{
						this.ctx.closePath();
						this.ctx.fill();
						this.ctx.beginPath();
						this.ctx.moveTo( thiscoords[i+1].x, thiscoords[i+1].y );
					}
				}
				else
				{
					this.ctx.lineTo( thiscoords[i].x, thiscoords[i].y );
				}
			}
			this.ctx.closePath();
			this.ctx.fill();
			
		    sin = Math.sin((Math.PI/4)*2);
		    cos = Math.cos((Math.PI/4)*2);
		    this.ctx.transform(cos, sin, -sin, cos, 0, 0);
		}
		this.ctx.scale(-1, 1);
		for (var z = 1; z <= 4; z++) {
			this.ctx.beginPath();
			this.ctx.moveTo( thiscoords[0].x, thiscoords[0].y );
			for (var i = 1; i < thislength; i++ )
			{
				if (thiscoords[i].y == 'X')
				{
					if (typeof(thiscoords[i+1]) != 'undefined')
					{
						this.ctx.closePath();
						this.ctx.fill();
						this.ctx.beginPath();
						this.ctx.moveTo( thiscoords[i+1].x, thiscoords[i+1].y );
					}
				}
				else
				{
					this.ctx.lineTo( thiscoords[i].x, thiscoords[i].y );
				}
			}
			this.ctx.closePath();
			this.ctx.fill();
			
		    sin = Math.sin((Math.PI/4)*2);
		    cos = Math.cos((Math.PI/4)*2);
		    this.ctx.transform(cos, sin, -sin, cos, 0, 0);
		}
	}
	this.init();
}






/*
function Cord(x, y) {
	this.x = x;
	this.y = y;
}

function Flake(id, text, ctx, maxr, cut) {
	this.max_radial = maxr;
	this.color;
	this.r = 0;
	this.x = 0;
	this.y = 0;
	this.id = id;
	this.text = text;
	this.ctx = ctx;
	this.angle = 0;
	this.cut = cut;
	this.height = 250;
	this.drift = Math.random();


	this.init = function() {
		width = this.ctx.canvas.width;
		height = this.ctx.canvas.height;
		this.x = (10 + Math.random() * (width - 2 * this.max_radial));
		this.y = -this.height; //-(10 + Math.random() * (height - 2 * this.max_radial));
		this.r = 0.2 * this.max_radial + Math.random() * (0.8 * this.max_radial);
		this.color = "rgba(255, 255, 255, " + .5 * ( height - this.max_radial ) / height + ")";
	}
	
	this.draw = function() {
		thislength = this.cut.length;
		thiscoords = this.cut;
		addx = 0;
		addy = 0;
		//console.log(this.id);
		this.ctx.globalCompositeOperation = "source-over";
		this.ctx.fillStyle = this.color;
		this.ctx.beginPath();
		this.ctx.arc(0, 0, 240, 0, Math.PI*2, false);
		this.ctx.fill();

		//this.ctx.globalCompositeOperation = "destination-out";

		this.ctx.fillStyle = 'black';
		
		for (var z = 1; z <= 4; z++) {
			this.ctx.beginPath();
			this.ctx.moveTo( thiscoords[0].x, thiscoords[0].y );
			for (var i = 1; i < thislength; i++ )
			{
				if (thiscoords[i].y == 'X')
				{

					if (typeof(thiscoords[i+1]) != 'undefined')
					{
						this.ctx.closePath();
						this.ctx.fill();
						this.ctx.beginPath();
						this.ctx.moveTo( thiscoords[i+1].x, thiscoords[i+1].y );
					}
				}
				else
				{
					this.ctx.lineTo( thiscoords[i].x, thiscoords[i].y );
				}
			}
			this.ctx.closePath();
			this.ctx.fill();
			
		    sin = Math.sin((Math.PI/4)*2);
		    cos = Math.cos((Math.PI/4)*2);
		    this.ctx.transform(cos, sin, -sin, cos, 0, 0);
		}
		this.ctx.scale(-1, 1);
		for (var z = 1; z <= 4; z++) {
			this.ctx.beginPath();
			this.ctx.moveTo( thiscoords[0].x, thiscoords[0].y );
			for (var i = 1; i < thislength; i++ )
			{
				if (thiscoords[i].y == 'X')
				{
					if (typeof(thiscoords[i+1]) != 'undefined')
					{
						this.ctx.closePath();
						this.ctx.fill();
						this.ctx.beginPath();
						this.ctx.moveTo( thiscoords[i+1].x, thiscoords[i+1].y );
					}
				}
				else
				{
					this.ctx.lineTo( thiscoords[i].x, thiscoords[i].y );
				}
			}
			this.ctx.closePath();
			this.ctx.fill();
			
		    sin = Math.sin((Math.PI/4)*2);
		    cos = Math.cos((Math.PI/4)*2);
		    this.ctx.transform(cos, sin, -sin, cos, 0, 0);
		}
	}

	this.init();
}
*/
