var canvas = null,
	ctx = null,
	b_canvas = null,
	b_ctx = null,
	s_canvas = null,
	s_ctx = null,
	flakes = [],
	flake_timer = null,
	save_timer = null,
	max_flakes = 20,
	max_radius = 10,
	mouseX = 0,
	mouseY = 0,
	matchedflake = -1,
	current_view = '';

function init()
{
	canvas = document.getElementById("snow");
	ctx = canvas.getContext("2d");
	// resize canvas to match window size
	ctx.canvas.width  = window.innerWidth;
	ctx.canvas.height = window.innerHeight;
	$(canvas).mousemove(function(e) {
		mouseX = e.pageX;
		mouseY = e.pageY;
	});

	$(canvas).mouseout(function(e) {
		mouseX = -1;
		mouseY = -1;
	});

	b_canvas = document.createElement("canvas");
	b_ctx = b_canvas.getContext("2d");
	b_ctx.canvas.width = ctx.canvas.width;
	b_ctx.canvas.height = ctx.canvas.height;

	// set timer to add flakes every 200 ms
	flake_timer = setInterval(addFlake, 300);
	// draw scene
	Draw();
	// start animation 30 frames per second
	setInterval(function() {
		animate();
	}, 1000/60);
}

function animate()
{
	Update();
	Draw();
}

function Update()
{
	for (var i = 0; i < flakes.length; i++)
	{
		if (flakes[i].y < ctx.canvas.height)
		{
			flakes[i].y += flakes[i].speed;
			if (flakes[i].y >= ctx.canvas.height)
			{
				flakes[i].y = -5;
			}
			flakes[i].x += flakes[i].drift;
			if (flakes[i].x > ctx.canvas.width)
			{
				flakes[i].x = 0;
			}
		}
	}
	// for every flake check if it matches cursor
	// separate loop as we need to break
	// only if we are viewing home page
	if (current_view == 'home')
	{
		for (var i = 0; i < flakes.length; i++)
		{
			if (((flakes[i].x - flakes[i].r) < mouseX) &&
				((flakes[i].x + flakes[i].r) > mouseX) &&
				((flakes[i].y - flakes[i].r) < mouseY) &&
				((flakes[i].y + flakes[i].r) > mouseY) &&
				mouseX > 0 && mouseY > 0)
			{
				$(canvas).css('cursor','pointer');
				if (!flakes[i].drawn && document.getElementById('info-box') != null)
				{
					$('#info-box').remove();
				}
				if (!flakes[i].drawn && document.getElementById('info-box') == null)
				{
					var p_container = document.createElement('div');
					p_container.setAttribute('id', 'info-box');
					p_container.setAttribute('style', 'display: none;');
					var p_canvas = document.createElement("canvas");
					p_canvas.width = 200;
					p_canvas.height = 200;
					$(p_canvas).click(function() {
						if(history.pushState && history.replaceState)
						{
							change_view(["", "flake", flakes[i].id]);
						}
					});
					p_ctx = p_canvas.getContext("2d");
					p_ctx.drawImage(flakes[i].t_canvas, 0, 0, 200, 200);
					p_container.appendChild(p_canvas);
					$('body').append(p_container);
					reset_drawing_states();
					flakes[i].drawn = true;
					$('#info-box').show().delay(3000).slideUp(300);
				}
				matchedflake = i;
				break;
			}
			else
			{
				matchedflake = -1;
				$(canvas).css('cursor','auto');
			}
		}
	}
}

function find_flake(id)
{
	for (var i = 0; i < flakes.length; i++)
	{
		if (flakes[i].id == id)
		{
			return flakes[i];
			break;
		}
	}
	return false;
}

function reset_drawing_states()
{
	for (var i = 0; i < flakes.length; i++)
	{
		flakes[i].drawn = false;
	}
}

function Draw()
{
	ctx.save();
	blank();
	for (var i = 0; i < flakes.length; i++) {
		b_ctx.drawImage(flakes[i].t_canvas, flakes[i].x, flakes[i].y, flakes[i].r, flakes[i].r);
	}
	// copy the entire rendered image from the buffer canvas to the visible one
	ctx.drawImage(b_canvas, 0, 0, b_canvas.width, b_canvas.height);
	ctx.restore();
}

function blank()
{
	b_ctx.clearRect(0, 0, b_ctx.canvas.width, b_ctx.canvas.height);
	var background = b_ctx.createLinearGradient(0, 0, 0, b_ctx.canvas.height);
	background.addColorStop(0, '#0D5995');
	background.addColorStop(1, '#1096D5');
	b_ctx.fillStyle = background;
	b_ctx.fillRect(0,0, b_ctx.canvas.width, b_ctx.canvas.height);
}

function addFlake(fid) {
	var str = '';
	if (parseInt(fid) > 0)
	{
		str = '?fid=' + fid;
	}
	$.ajax({
		url: base_url + 'get_flake.php' + str,
		success: function( data )
		{
			var fcount = flakes.length;
			flakes[fcount] = new Flake(fcount, jQuery.parseJSON( data ));
		}
	});
	// clear timer so no more flakes are added
	if (flakes.length >= max_flakes)
	{
		clearInterval(flake_timer);
	}
}


function Flake(id, flake_info)
{
	this.x = 0; //Math.round(Math.random() * ctx.canvas.width);
	this.y = 0;
	this.r = 0;
	this.drift = Math.random();
	this.speed = Math.round(Math.random() * 2) + 1;
	this.height = 250;
	this.id = flake_info.id;
	this.max_radial = max_radius;
	this.cut = jQuery.parseJSON(flake_info.coords);
	this.angle = 0;
	this.fid = flake_info.id;
	this.name = flake_info.name;
	this.message = flake_info.message;
	this.color = null;

	this.t_canvas = null;
	this.t_ctx = null;

	this.init = function()
	{
		this.t_canvas = document.createElement("canvas");
		this.t_canvas.width = 500;
		this.t_canvas.height = 500;
		this.t_ctx = this.t_canvas.getContext("2d");

		var width = ctx.canvas.width;
		var height = ctx.canvas.height;
		this.x = (10 + Math.random() * (width - 2 * this.max_radial));
		this.y = -this.height;
		this.r = 1.5 * this.max_radial + Math.random() * (0.8 * this.max_radial);
		this.color = "rgba(255, 255, 255, " + .5 * ( height - this.max_radial ) / height + ")";

		this.draw();
	}

	this.draw = function()
	{
		this.t_ctx.translate(250, 250);
		thislength = this.cut.length;
		thiscoords = this.cut;
		addx = 0;
		addy = 0;

		this.t_ctx.fillStyle = 'white';
		this.t_ctx.beginPath();
		this.t_ctx.arc(0, 0, 240, 0, Math.PI*2, false);
		this.t_ctx.fill();
		this.t_ctx.globalCompositeOperation = 'destination-out';

		this.t_ctx.fillStyle = 'black';
		for (var z = 1; z <= 4; z++) {
			this.t_ctx.beginPath();
			this.t_ctx.moveTo( thiscoords[0].x, thiscoords[0].y );
			for (var i = 1; i < thislength; i++ )
			{
				if (thiscoords[i].y == 'X')
				{
					if (typeof(thiscoords[i+1]) != 'undefined')
					{
						this.t_ctx.closePath();
						this.t_ctx.fill();
						this.t_ctx.beginPath();
						this.t_ctx.moveTo( thiscoords[i+1].x, thiscoords[i+1].y );
					}
				}
				else
				{
					this.t_ctx.lineTo( thiscoords[i].x, thiscoords[i].y );
				}
			}
			this.t_ctx.closePath();
			this.t_ctx.fill();
			sin = Math.sin((Math.PI/4)*2);
			cos = Math.cos((Math.PI/4)*2);
			this.t_ctx.transform(cos, sin, -sin, cos, 0, 0);
		}
		this.t_ctx.scale(-1, 1);
		for (var z = 1; z <= 4; z++) {
			this.t_ctx.beginPath();
			this.t_ctx.moveTo( thiscoords[0].x, thiscoords[0].y );
			for (var i = 1; i < thislength; i++ )
			{
				if (thiscoords[i].y == 'X')
				{
					if (typeof(thiscoords[i+1]) != 'undefined')
					{
						this.t_ctx.closePath();
						this.t_ctx.fill();
						this.t_ctx.beginPath();
						this.t_ctx.moveTo( thiscoords[i+1].x, thiscoords[i+1].y );
					}
				}
				else
				{
					this.t_ctx.lineTo( thiscoords[i].x, thiscoords[i].y );
				}
			}
			this.t_ctx.closePath();
			this.t_ctx.fill();
			sin = Math.sin((Math.PI/4)*2);
			cos = Math.cos((Math.PI/4)*2);
			this.t_ctx.transform(cos, sin, -sin, cos, 0, 0);
		}
	}
	this.init();
}

var AboutBtn = function()
{
	var container = document.createElement("div");
	container.setAttribute('id', 'about-button');
	container.setAttribute('style', 'font-size: 30px; position: absolute; top: 50px; left: 50px; z-index: 3;');
	btt = document.createElement('a');
	btt.innerHTML = '<i class="icon-info-sign icon-large"></i>';
	btt.setAttribute('href', 'javascript:void(0);');
	btt.setAttribute('style', 'text-decoration: none; color: #000;');
	btt.addEventListener("click", function() {
		change_view(['', 'about']);
	});
	container.appendChild(btt);

	// automatically output this element to body 
	document.body.appendChild(container);
}

var AboutSection = function() {
	this.container = document.createElement("div");
	this.container.setAttribute('id', 'about-box');
	this.container.setAttribute('style', 'position: absolute; top: 50px; width: 600px; border: 1px solid #000; border-radius: 15px; z-index: 3; background: rgba(0,0,0, 0.2); display: none;');
	this.padding = document.createElement("div");
	this.padding.setAttribute('class', 'padding');
	this.padding.setAttribute('style', 'position: relative; padding: 25px;');
	this.h1 = document.createElement("h1");
	this.h1.innerHTML = "Welcome to Let it Snow.";
	this.padding.appendChild(this.h1);
	this.p = document.createElement("p");
	this.p.innerHTML = 'I started this project in 2011, but there was no snow at that time so I abandoned it, but I focused on it back again in 2013, fixed some issues and here it is.<br />You can create your custom snowflakes here and watch what aothers created.<br />Enjoy the snow!<br /><br /><strong>Tomasz Turczynski</strong>';
	this.padding.appendChild(this.p);
	this.close_btt = document.createElement("a");
	this.close_btt.innerHTML = '<i class="icon-remove-circle icon-large"></i>';
	this.close_btt.setAttribute('href', 'javascript:void(0);');
	this.close_btt.setAttribute('style', 'color: #000; text-decoration: none; font-size: 20px;');
	this.close_btt.addEventListener("click", function() {
		change_view(['', 'home']);
	});
	this.close = document.createElement("p");
	this.close.setAttribute('style', 'text-align: right; position: absolute; top: 20px; right: 20px;');
	this.close.appendChild(this.close_btt);
	this.padding.appendChild(this.close);
	this.container.appendChild(this.padding);
	this.reposition = function()
	{
		var this_w = $(this.container).width();
		var html_w = $('html').width();
		var left = Math.floor((html_w / 2) - (this_w / 2));
		this.container.style.left = left + 'px';
	}
	this.show = function() {
		this.container.style.display = "block";
		this.reposition();
	};
	this.hide = function() {
		this.container.style.display = "none";
	};

	// automatically output this element to body 
	document.body.appendChild(this.container);
};

var DesignBtn = function()
{
	var container = document.createElement("div");
	container.setAttribute('id', 'design-button');
	container.setAttribute('style', 'font-size: 30px; position: absolute; top: 100px; left: 50px; z-index: 3;');
	btt = document.createElement('a');
	btt.innerHTML = '<i class="icon-edit icon-large"></i>';
	btt.setAttribute('href', 'javascript:void(0);');
	btt.setAttribute('style', 'text-decoration: none; color: #000;');
	btt.addEventListener("click", function() {
		change_view(['', 'design']);
	});
	container.appendChild(btt);

	// automatically output this element to body 
	document.body.appendChild(container);
}

var DrawingCord = function(x, y) {
	this.x = x;
	this.y = y;
}

var DrawingCanvas = function()
{
	this.container = document.createElement("div");
	this.container.setAttribute('id', 'design-box');
	this.container.setAttribute('style', 'position: absolute; top: 50px; border: 1px solid #000; border-radius: 15px; background: rgba(0,0,0, 0.2); z-index: 3; display: none;');
	this.padding = document.createElement("div");
	this.padding.setAttribute('class', 'padding');
	this.padding.setAttribute('style', 'position: relative;');

	this.canvas = document.createElement('canvas');
	this.canvas.width = 500;
	this.canvas.height = 500;
	this.canvas.id = "draw";
	this.context = this.canvas.getContext('2d');

	this.Xpos = 0;
	this.Ypos = 0;
	this.zx = 250 + (240*Math.cos(Math.PI/4));
	this.zy = 250 + (240*Math.sin(Math.PI/4));
	this.started = false;
	this.startX = 0;
	this.startY = 0;
	this.coords = [];
	this.drawing = false;
	this.saved = false;

	$(this.canvas).click(function(e) {
		var that = design;
		if (that.drawing)
		{
			var offset = $(that.canvas).offset();
			that.canvas.style.cursor = 'crosshair';

			that.Xpos = e.pageX - offset.left;
			that.Ypos = e.pageY - offset.top;
			if (that.Xpos > 250 && that.Xpos <= that.zx) {
				var adj = that.Xpos - 250;
				if (adj > 0) {
					var opp = adj * Math.tan(Math.PI/4);
					var y = opp + 250;
					if (that.Ypos > y) {
						console.log('changing y pos from ' + that.Ypos + ' to ' + y);
						that.Ypos = y;
					}
					if (that.Ypos < 250) {
						console.log('changing y pos from ' + that.Ypos + ' to ' + 250);
						that.Ypos = 250;
					}
				}
			}
			if (!that.started)
			{
				that.context.beginPath();
				that.context.globalCompositeOperation = "destination-out";
				that.startX = that.Xpos;
				that.startY = that.Ypos;
				that.context.moveTo(that.Xpos, that.Ypos);
				that.coords.push(new DrawingCord(that.Xpos, that.Ypos));
				that.started = true;
			}
			else
			{
				that.context.lineTo(that.Xpos, that.Ypos);
				that.coords.push(new DrawingCord(that.Xpos, that.Ypos));
				that.context.stroke();
			}
		}
	});

	$(this.canvas).dblclick(function(e) {
		var that = design;
		if (that.drawing)
		{
			var offset = $(that.canvas).offset();

			that.Xpos = e.pageX - offset.left;
			that.Ypos = e.pageY - offset.top;
			if (that.Xpos > 250 && that.Xpos <= that.zx) {
				var adj = that.Xpos - 250;
				if (adj > 0) {
					var opp = adj * Math.tan(Math.PI/4);
					var y = opp + 250;
					if (that.Ypos > y) {
						console.log('changing y pos from ' + that.Ypos + ' to ' + y);
						that.Ypos = y;
					}
					if (that.Ypos < 250) {
						console.log('changing y pos from ' + that.Ypos + ' to ' + 250);
						that.Ypos = 250;
					}
				}
			}
			if (that.started)
			{
				that.context.lineTo(that.Xpos, that.Ypos);
				that.coords.push(new DrawingCord(that.Xpos, that.Ypos));
				that.context.lineTo(that.startX, that.startY);
				that.coords.push(new DrawingCord(that.startX, that.startY));
				that.context.closePath();
				that.context.fill();
				that.started = false;
				that.coords.push(new DrawingCord('X', 'X'));
				that.canvas.style.cursor = 'auto';
			}
		}
	});

	this.clear = function()
	{
		this.coords = new Array();
		this.context.fillStyle = '#ffffff';
		this.context.clearRect(0, 0, this.canvas_width, this.canvas_height);
		this.canvas.width = this.canvas.width;
	
		this.context.fillStyle = 'white';
		this.context.beginPath();
		this.context.arc(250, 250, 240, 0, (Math.PI/4), false);
		this.context.lineTo(250, 250);
		this.context.closePath();
		this.context.fill();
		this.context.save();

		this.drawing = true;
		this.saved = false;
	}

	this.preview = function()
	{
		if (this.drawing)
		{
			// 45 degrees
			var degree = Math.PI/4;
			this.context.restore();
			// set rotation point
			this.context.translate(250, 250);

			for (i = 1; i <= 7; i++) {
				this.context.rotate(degree);
				this.context.beginPath();
				this.context.arc(0, 0, 240, 0, degree, false);
				this.context.lineTo(0, 0);
				this.context.closePath();
				this.context.fill();
			}
			this.context.restore();

			this.context.scale(-1, 1);
			this.context.rotate(3 * degree);
			this.copy_slice(this.context, this.coords, 250, 250);
			this.context.restore();

			this.context.scale(-1, 1);
			this.context.rotate(6 * degree);
			this.copy_slice(this.context, this.coords, 250, 250);
			this.context.restore();

			this.context.scale(-1, 1);
			this.context.rotate(8 * degree);
			this.copy_slice(this.context, this.coords, 250, 250);
			this.context.restore();

			this.context.scale(-1, -1);
			this.context.rotate(6 * degree);
			this.copy_slice(this.context, this.coords, 250, 250);
			this.context.restore();

			this.context.scale(1, -1);
			this.context.rotate(2 * degree);
			this.copy_slice(this.context, this.coords, 250, 250);
			this.context.restore();

			this.context.scale(-1, -1);
			this.context.rotate(2 * degree);
			this.copy_slice(this.context, this.coords, 250, 250);
			this.context.restore();

			this.context.scale(-1, 1);
			this.context.rotate(6 * degree);
			this.copy_slice(this.context, this.coords, 250, 250);
			this.context.restore();
			this.drawing = false;
		}
	}

	this.copy_slice = function(dctx, coords, addx, addy)
	{
		// first create copy of the slice
		dctx.globalCompositeOperation = "destination-out";
	
		if (coords.length > 0)
		{
			dctx.fillStyle = 'black';
			dctx.beginPath();
			dctx.moveTo(coords[0].x-addx, coords[0].y-addy);
			for ( i = 1; i < coords.length; i++ )
			{
				if (coords[i].y == 'X')
				{
					dctx.closePath();
					dctx.fill();
					console.log('resseting line');
					dctx.beginPath();
					if (typeof(coords[i+1]) != 'undefined')
					{
						dctx.moveTo( (coords[i+1].x-addx), (coords[i+1].y-addy) );
					}
				}
				else
				{
					dctx.lineTo((coords[i].x-addx), (coords[i].y-addy));
					console.log('drawing line to ' + (coords[i].x-addx) + 'x' + (coords[i].y-addy));
				}
			}
			dctx.closePath();
			dctx.fill();	
		}
		dctx.restore();
	}

	this.save = function(name, message)
	{
		if (this.coords.length > 0 && name != '' && !this.saved)
		{
			//console.log(this.coords);
			$.post(base_url + 'save_flake.php', { coords: this.coords, name: name, message: message }, function(ret) {
				var flake_id = parseInt(ret);
				if (flake_id > 0)
				{
					design.saved = true;
					change_view(['', 'flake', flake_id]);
					addFlake(flake_id);
					return true;
				}
			});
		}
		return false;
	}

	this.buttons = function()
	{
		var close_btt = document.createElement("a");
		close_btt.innerHTML = '<i class="icon-remove-circle icon-large"></i>';
		close_btt.setAttribute('href', 'javascript:void(0);');
		close_btt.setAttribute('style', 'color: #000; text-decoration: none; font-size: 20px;');
		close_btt.addEventListener("click", function() {
			change_view(['', 'home']);
		});
		var close = document.createElement("p");
		close.setAttribute('style', 'text-align: right; position: absolute; top: 20px; right: 20px;');
		close.appendChild(close_btt);

		var reset_btt = document.createElement("div");
		reset_btt.innerHTML = '<i class="icon-retweet icon-large"></i>';
		reset_btt.setAttribute('style', 'position: absolute; top: 190px; right: -60px; color: #000; text-decoration: none; font-size: 40px; cursor: pointer;');
		reset_btt.addEventListener("click", function() {
			design.clear();
		});

		var preview_btt = document.createElement("div");
		preview_btt.innerHTML = '<i class="icon-ok-sign icon-large"></i>';
		preview_btt.setAttribute('style', 'position: absolute; top: 270px; right: -60px; color: #000; text-decoration: none; font-size: 40px; cursor: pointer;');
		preview_btt.addEventListener("click", function(e) {
			design.preview();
			$('#submit-form').css('display', 'block');
		});
		this.padding.appendChild(close);
		this.padding.appendChild(reset_btt);
		this.padding.appendChild(preview_btt);
	}

	this.form = function()
	{
		// container of the form
		var container = document.createElement("div");
		container.setAttribute('id', 'submit-form');
		container.setAttribute('style', 'display: none; box-shadow: 0 0 20px #000; background: #fff url(' + base_url + 'images/mail_bg.png) top left repeat-x; position: absolute; top: 190px; right: -270px; width: 260px;');
		// padding div to keep some paddings from background
		var padding = document.createElement("div");
		padding.setAttribute('class', 'padding');
		padding.setAttribute('style', 'padding: 20px 0px; background: url(' + base_url + 'images/mail_bg.png) bottom left repeat-x; position: relative;');
		// form
		var form = document.createElement("form");
		form.setAttribute('name', 'submit_form');
		form.setAttribute('onsubmit', 'return false;');
		var input_submit = document.createElement("input");
		input_submit.setAttribute('type', 'submit');
		input_submit.setAttribute('style', 'display: none;');
		input_submit.setAttribute('id', 'submit-button');
		// row one of the form
		var row1 = document.createElement("div");
		row1.setAttribute('style', 'border-bottom: 1px dotted #ccc; padding: 5px;');
		var label1 = document.createElement("label");
		label1.setAttribute('for', 'field_name');
		label1.setAttribute('style', 'color: #666; font-size: 11px; font-family: arial, sans-serif; width: 85px; display: block; float: left;');
		label1.innerHTML = 'Your name:';
		var input1 = document.createElement("input");
		input1.setAttribute('text', 'text');
		input1.setAttribute('name', 'name');
		input1.setAttribute('id', 'field_name');
		input1.setAttribute('style', 'width: 150px;');
		input1.setAttribute('pattern', '[A-Za-z- ]{0,20}');
		input1.setAttribute('title', 'Only letters, spaces and dashes, minimum 3, maximum 20.');
		input1.onkeyup = function()
		{
			document.getElementById('submit-button').click();
		}
		row1.appendChild(label1);
		row1.appendChild(input1);
		// row two of the form
		var row2 = document.createElement("div");
		row2.setAttribute('style', 'border-bottom: 1px dotted #ccc; padding: 5px;');
		var label2 = document.createElement("label");
		label2.setAttribute('for', 'field_message');
		label2.setAttribute('style', 'color: #666; font-size: 11px; font-family: arial, sans-serif; width: 85px; display: block; float: left;');
		label2.innerHTML = 'Your message:';
		var input2 = document.createElement("input");
		input2.setAttribute('text', 'text');
		input2.setAttribute('name', 'message');
		input2.setAttribute('id', 'field_message');
		input2.setAttribute('style', 'width: 150px;');
		input2.setAttribute('pattern', '[A-Za-z- ]{0,20}');
		input2.setAttribute('title', 'Only letters, spaces and dashes, minimum 3, maximum 20.');
		row2.appendChild(label2);
		row2.appendChild(input2);
		// row three of the form with all the buttons
		var row3 = document.createElement("div");
		row3.setAttribute('style', 'text-align: center; padding-top: 6px;');
		var btt_save = document.createElement("a");
		btt_save.setAttribute('href', '#');
		btt_save.setAttribute('id', 'submit-form-save');
		btt_save.setAttribute('style', 'text-decoration: none; color: #000; text-transform: uppercase; font-family: arial, sans-serif; font-size: 12px;');
		btt_save.innerHTML = '<i class="icon-ok-circle icon-large" style="font-size: 14px;"></i> Save';
		btt_save.addEventListener("click", function(e) {
			design.save($('#field_name').attr('value'), $('#field_message').attr('value'));
			var overlay = document.createElement("div");
			overlay.setAttribute('class', 'overlay');
			overlay.setAttribute('style', 'top: 0; left: 0; width: 100%; height: 100%; position: absolute; z-index:5; background: rgba(0,0,0, 0.3);');
			overlay.innerHTML = '<p>Saving...</p>';
			$('#submit-form .padding').append(overlay);
			save_timer = setInterval(function() {
				if (design.saved)
				{
					clearInterval(save_timer);
					$('#field_name').attr('value', '');
					$('#field_message').attr('value', '');
					$('#submit-form .padding .overlay').remove();
					$('#submit-form').css('display', 'none');
				}
			}, 10);
		});
		var btt_cancel = document.createElement("a");
		btt_cancel.setAttribute('href', '#');
		btt_cancel.setAttribute('id', 'submit-form-cancel');
		btt_cancel.setAttribute('style', 'text-decoration: none; color: #999; text-transform: uppercase; font-family: arial, sans-serif; font-size: 12px;');
		btt_cancel.innerHTML = '<i class="icon-remove-circle icon-large" style="font-size: 14px;"></i> Cancel';
		btt_cancel.addEventListener("click", function(e) {
			$('#field_name').attr('value', '');
			$('#field_message').attr('value', '');
			$('#submit-form').css('display', 'none');
			design.saved = false;
		});
		var span = document.createElement("span");
		span.setAttribute('style', 'padding: 0px 10px;');
		span.innerHTML = '|';
		row3.appendChild(btt_save);
		row3.appendChild(span);
		row3.appendChild(btt_cancel);
		// add everything to main container
		form.appendChild(row1);
		form.appendChild(row2);
		form.appendChild(row3);
		form.appendChild(input_submit);
		padding.appendChild(form);
		container.appendChild(padding);
		// add everything to this.padding
		this.padding.appendChild(container);
	}

	this.reposition = function()
	{
		var this_w = $(this.container).width();
		var html_w = $('html').width();
		var left = Math.floor((html_w / 2) - (this_w / 2));
		this.container.style.left = left + 'px';
	}

	this.show = function() {
		this.clear();
		this.container.style.display = "block";
		this.reposition();
	};
	this.hide = function() {
		this.container.style.display = "none";
	};

	//fixes a problem where double clicking causes text to get selected on the canvas
	this.canvas.addEventListener('selectstart', function(e) { e.preventDefault(); return false; }, false);
	this.clear();
	// output buttons to DOM
	this.buttons();
	// output form to DOM
	this.form();

	// automatically output this element to body 
	this.padding.appendChild(this.canvas);
	this.container.appendChild(this.padding);
	document.body.appendChild(this.container);
}

var ViewSection = function() {
	this.container = null;

	this.reposition = function()
	{
		var this_w = $(this.container).width();
		var html_w = $('html').width();
		var left = Math.floor((html_w / 2) - (this_w / 2));
		this.container.style.left = left + 'px';
	}
	this.show = function(id) {
		var this_flake =  find_flake(id);
		// if flake was not found, load it
		if (!this_flake)
		{
			$.ajax({
				url: base_url + 'get_flake.php?fid=' + id,
				success: function( data )
				{
					var loaded_flake = jQuery.parseJSON( data );
					if (loaded_flake.id > 0)
					{
						flakes.push(new Flake(flakes.length, loaded_flake));
						details.show(id);
					}
					else
					{
						console.log('flake does not exist');
						change_view(['', 'home']);
					}
				}
			});
		}
		else
		{
			this.container = document.createElement("div");
			this.container.setAttribute('id', 'view-box');
			this.container.setAttribute('style', 'position: absolute; border-radius: 15px; top:50px; border: 1px solid #000; background: rgba(0,0,0,0.2); z-index: 5;');
			this.padding = document.createElement("div");
			this.padding.setAttribute('class', 'padding');
			this.padding.setAttribute('style', 'position: relative; padding: 20px;');
			this.h1 = document.createElement("h1");
			this.name = document.createElement("p");
			this.name.setAttribute('class', 'name');
			this.message = document.createElement("p");
			this.message.setAttribute('class', 'message');
			this.padding.appendChild(this.h1);
			this.padding.appendChild(this.name);
			this.container.appendChild(this.padding);
			this.close_btt = document.createElement("a");
			this.close_btt.innerHTML = '<i class="icon-remove-circle icon-large"></i>';
			this.close_btt.setAttribute('href', 'javascript:void(0);');
			this.close_btt.setAttribute('style', 'color: #000; text-decoration: none; font-size: 20px;');
			this.close_btt.addEventListener("click", function() {
				change_view(['', 'home']);
			});
			this.close = document.createElement("p");
			this.close.setAttribute('style', 'text-align: right; position: absolute; top: 20px; right: 20px;');
			this.close.appendChild(this.close_btt);
			this.padding.appendChild(this.close);

			this.h1.innerHTML = 'Flake #' + id;
			if (this_flake.name != '')
			{
				this.message.innerHTML = '<em>"' + this_flake.message + '"</em>';
				this.padding.appendChild(this.message);
			}
			this.name.innerHTML = 'by ' + this_flake.name;
			this.canvas = document.createElement("canvas");
			this.canvas.setAttribute('class', 'flake-rotate');
			this.canvas.width = 500;
			this.canvas.height = 500;
			this.ctx = this.canvas.getContext("2d");
			this.ctx.drawImage(this_flake.t_canvas, 0, 0, 500, 500);
			this.padding.appendChild(this.canvas);
			document.body.appendChild(this.container);
			this.reposition();
		}
	};
	this.hide = function() {
		this.container = null;
		$('#view-box').remove();
	};
};

var about_btn = new AboutBtn;
var design_btn = new DesignBtn;
var design = new DrawingCanvas;
var about = new AboutSection;
var details = new ViewSection;

function change_view(view)
{
	if (view[1] == '')
	{
		view[1] = 'home';
	}
	if (current_view != view[1])
	{
		switch(view[1])
		{
			case 'about':
				details.hide();
				design.hide();
				about.show();
				window.history.pushState({'view': 'about'}, 'About', base_url + 'about');
				current_view = 'about';
				break;
			case 'design':
				details.hide();
				about.hide();
				design.show();
				window.history.pushState({'view': 'design'}, 'Design', base_url + 'design');
				current_view = 'design';
				break;
			case 'flake':
				design.hide();
				about.hide();
				details.show(view[2]);
				window.history.pushState({'view': 'flake', 'id': view[2]}, 'Flake ' + view[2], base_url + 'flake/' + view[2]);
				current_view = 'flake';
				break;
			default:
				details.hide();
				design.hide();
				about.hide();
				window.history.pushState({'view': 'home'}, 'Home', base_url);
				current_view = 'home';
				break;
		}
	}
}

var handlePath = function() {
	var path = window.location.pathname;
	path = path.replace(base_url, '/');
	var b = path.split("/");
	change_view(b);
};
window.addEventListener("popstate", function() {
	handlePath()
});
handlePath();

$(document).ready(function() {
	init();
});


(function(d, s, id) {var js, fjs = d.getElementsByTagName(s)[0];if (d.getElementById(id)) return;js = d.createElement(s); js.id = id;js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=341716419269019";fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));
