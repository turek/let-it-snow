var mouseX = 0;
var mouseY = 0;
var matchedflake = -1;
var ctx;
var fcount  = 100;
var flakes  = new Array();
var fmaxr   = 10;



function Cord(x, y) {
	this.x = x;
	this.y = y;
}

function Flake(id, text, ctx, maxr, cut) {
	this.max_radial = maxr;
	this.color;
	this.r = 0;
	this.x = 0;
	this.y = 0;
	this.id = id;
	this.text = text;
	this.ctx = ctx;
	this.angle = 0;
	this.cut = cut;
	this.height = 250;

	this.init = function() {
		width = this.ctx.canvas.width;
		height = this.ctx.canvas.height;
		this.x = (10 + Math.random() * (width - 2 * this.max_radial));
		this.y = -this.height; //-(10 + Math.random() * (height - 2 * this.max_radial));
		this.r = 0.2 * this.max_radial + Math.random() * (0.8 * this.max_radial);
		this.color = "rgba(255, 255, 255, " + .5 * ( height - this.max_radial ) / height + ")";
	}
	
	this.draw = function() {
		thislength = this.cut.length;
		thiscoords = this.cut;
		addx = 0;
		addy = 0;
		//console.log(this.id);
		this.ctx.globalCompositeOperation = "source-over";
		this.ctx.fillStyle = this.color;
		this.ctx.beginPath();
		this.ctx.arc(0, 0, 240, 0, Math.PI*2, false);
		this.ctx.fill();

		//this.ctx.globalCompositeOperation = "destination-out";

		this.ctx.fillStyle = 'black';
		
		for (var z = 1; z <= 4; z++) {
			this.ctx.beginPath();
			this.ctx.moveTo( thiscoords[0].x, thiscoords[0].y );
			for (var i = 1; i < thislength; i++ )
			{
				if (thiscoords[i].y == 'X')
				{

					if (typeof(thiscoords[i+1]) != 'undefined')
					{
						this.ctx.closePath();
						this.ctx.fill();
						this.ctx.beginPath();
						this.ctx.moveTo( thiscoords[i+1].x, thiscoords[i+1].y );
					}
				}
				else
				{
					this.ctx.lineTo( thiscoords[i].x, thiscoords[i].y );
				}
			}
			this.ctx.closePath();
			this.ctx.fill();
			
		    sin = Math.sin((Math.PI/4)*2);
		    cos = Math.cos((Math.PI/4)*2);
		    this.ctx.transform(cos, sin, -sin, cos, 0, 0);
		}
		this.ctx.scale(-1, 1);
		for (var z = 1; z <= 4; z++) {
			this.ctx.beginPath();
			this.ctx.moveTo( thiscoords[0].x, thiscoords[0].y );
			for (var i = 1; i < thislength; i++ )
			{
				if (thiscoords[i].y == 'X')
				{
					if (typeof(thiscoords[i+1]) != 'undefined')
					{
						this.ctx.closePath();
						this.ctx.fill();
						this.ctx.beginPath();
						this.ctx.moveTo( thiscoords[i+1].x, thiscoords[i+1].y );
					}
				}
				else
				{
					this.ctx.lineTo( thiscoords[i].x, thiscoords[i].y );
				}
			}
			this.ctx.closePath();
			this.ctx.fill();
			
		    sin = Math.sin((Math.PI/4)*2);
		    cos = Math.cos((Math.PI/4)*2);
		    this.ctx.transform(cos, sin, -sin, cos, 0, 0);
		}
	}

	this.init();
}

function Stage(flakes, ctx, maxr) {
	this.width = ctx.canvas.width;
	this.height = ctx.canvas.height;
	this.ctx = ctx;
	this.flakes = flakes;
	this.speed = 0.3;
	this.maxr = maxr;
	
	this.draw = function() {
		this.clear();
		
		for (var i = 0; i < this.flakes.length; i++ ) {
/*
			//console.log(this.flakes[i].coords);
				startangle = this.flakes[i].angle;
				endangle = startangle + Math.PI/4;
				this.ctx.fillStyle = this.flakes[i].color;

				thisx = -(this.flakes[i].x);
				
				console.log(thisx + ' x ' + this.flakes[i].y + ' / ' + this.flakes.length);
				
//				this.ctx.restore();
//			    sin = Math.sin(Math.PI/4);
//			    cos = Math.cos(Math.PI/4);
//			    this.ctx.translate(250, 250);
//			    this.ctx.transform(cos, sin, -sin, cos, 0, 0);
	
				//this.ctx.rotate(Math.PI/4);
				//this.ctx.translate(this.flakes[i].x, this.flakes[i].y);
	
//				this.ctx.beginPath();
//				this.ctx.arc(this.flakes[i].x, this.flakes[i].y, 240, startangle, Math.PI/4, false);
//				this.ctx.lineTo(this.flakes[i].x, this.flakes[i].y);
//				this.ctx.closePath();
//				this.ctx.fill();

				if (typeof(this.flakes[i].cut) != 'undefined') {
					thislength = this.flakes[i].cut.length;
					thiscoords = this.flakes[i].cut
					this.ctx.fillStyle = 'black';
					this.ctx.beginPath();
					addx = 0+thisx;
					addy = 0-this.flakes[i].y;
					
					this.ctx.moveTo(thiscoords[0].x-addx, thiscoords[0].y-addy);
					for ( i = 1; i < thislength; i++ )
					{
						if (thiscoords[i].y == 'X')
						{
							this.ctx.closePath();
							this.ctx.fill();
							//console.log('resseting line');
							this.ctx.beginPath();
							if (typeof(thiscoords[i+1]) != 'undefined')
							{
								this.ctx.moveTo( (thiscoords[i+1].x-addx), (thiscoords[i+1].y-addy) );
							}
						}
						else
						{
							this.ctx.lineTo((thiscoords[i].x-addx), (thiscoords[i].y-addy));
							//console.log('drawing line to ' + (thiscoords[i].x-addx) + 'x' + (thiscoords[i].y-addy));
						}
					}
					this.ctx.closePath();
					this.ctx.fill();
				}

*/				
				//this.flakes[i].angle += Math.PI/4;
				//this.ctx.translate(0, 0);
/*
//			pi = Math.PI/4;
//			pii = Math.PI/4 + pi;
			//for (j = 1; j <= 8; j++) {
			
//			this.ctx.translate(this.flakes[i].x, this.flakes[i].y);
//			this.ctx.rotate(Math.PI/4);
//			this.ctx.arc(0, 0, this.flakes[i].r, 0, Math.PI/4, false);
//			this.ctx.lineTo(0, 0);
			this.ctx.beginPath();
			this.ctx.arc(this.flakes[i].x, this.flakes[i].y, this.flakes[i].r, startangle, endangle, false);
			this.ctx.lineTo(this.flakes[i].x, this.flakes[i].y);
			this.ctx.closePath();
			this.ctx.fill();

			this.ctx.globalCompositeOperation = "destination-out";
			
			this.ctx.beginPath();
			this.ctx.arc(this.flakes[i].x+3, this.flakes[i].y, this.flakes[i].r/2, 0, 2 * Math.PI, false);
			this.ctx.fillStyle = "#1096D5";
			this.ctx.fill();	
			
			this.ctx.globalCompositeOperation = "source-over";
			// increase angle
			this.flakes[i].angle += Math.PI/4;

//				pi += pi;
//				pii += pi;
			//}
*/
//			this.ctx.globalCompositeOperation = 'source-over';
//			this.ctx.beginPath();
//			this.ctx.arc(this.flakes[i].x+3, this.flakes[i].y, this.flakes[i].r/2, 0, 2 * Math.PI, false);
//			this.ctx.fillStyle = "#1096D5";
//			this.ctx.fill();			
//			this.ctx.translate( this.flakes[i].x, this.flakes[i].y );
//			this.ctx.rotate( 55 * Math.PI/180 );
			
			
//			this.ctx.beginPath();
//			this.ctx.arc(this.flakes[i].x, this.flakes[i].y, this.flakes[i].r, 0, 2 * Math.PI, false);
//			this.ctx.fill();	
			
			this.ctx.save();
			this.ctx.translate( this.flakes[i].x, this.flakes[i].y );
			this.ctx.scale(0.04, 0.04);

		    this.flakes[i].draw();
		    
			this.ctx.restore();
		}
	}

	this.checkFlakesPosition = function() {
		for (var i = 0; i < this.flakes.length; i++ ) {
			if ( this.flakes[i].y > this.height + this.maxr ) {
				this.flakes[i].y = -this.flakes[i].height;
				this.flakes[i].r = Math.random() * this.maxr;
			}
		}
	}

	this.fall = function() {
		for (var i = 0; i < this.flakes.length; i++ ) {
			this.flakes[i].y += this.speed * this.flakes[i].r;
		}
	}
	
	this.clear = function() {
		this.ctx.save();
		this.ctx.setTransform(1,0,0,1,0,0);
		this.ctx.clearRect(0,0, this.width, this.height);
		background = this.ctx.createLinearGradient(0, 0, 0, this.height);
		background.addColorStop(0, '#0D5995');
		background.addColorStop(1, '#1096D5');
		this.ctx.fillStyle = background;
		this.ctx.fillRect(0, 0, this.width, this.height);
		this.ctx.restore();
	}
}

var coords = new Array();
var cuts = new Array();
var dctx = null;
var started = false;
var startx = 0;
var starty = 0;
var draw_canvas_width = 500;
var draw_canvas_height = 500;
var zx = 250 + (240*Math.cos(Math.PI/4));
var zy = 250 + (240*Math.sin(Math.PI/4));
var draw_canvas;
var draw_context;

function prepare_drawing_canvas()
{
	draw_canvas = document.createElement('canvas');
	draw_canvas.width = draw_canvas_width;
	draw_canvas.height = draw_canvas_height;
	draw_canvas.id = "draw";
	draw_context = draw_canvas.getContext('2d');

	$('#canvas').after(draw_canvas);

	$('#draw').click(function(e) {
		thisXpos = e.pageX-200;
		thisYpos = e.pageY-100;
		if (thisXpos > 250 && thisXpos <= zx) {
			adj = thisXpos - 250;
			if (adj > 0) {
				opp = adj * Math.tan(Math.PI/4);
				y = opp + 250;
				if (thisYpos > y) {
					console.log('changing y pos from ' + thisYpos + ' to ' + y);
					thisYpos = y;
				}
				if (thisYpos < 250) {
					console.log('changing y pos from ' + thisYpos + ' to ' + 250);
					thisYpos = 250;
				}
			}
		}
		if (!started) {
			draw_context.beginPath();
			draw_context.globalCompositeOperation = "destination-out";
			startx = thisXpos;
			starty = thisYpos;
			draw_context.moveTo(thisXpos, thisYpos);
			coords.push(new Cord(thisXpos, thisYpos));
		    started = true;
		} else {
			draw_context.lineTo(thisXpos, thisYpos);
			coords.push(new Cord(thisXpos, thisYpos));
			draw_context.stroke();
		}
	});

	$('#draw').dblclick(function(e) {
		thisXpos = e.pageX-200;
		thisYpos = e.pageY-100;
		if (thisXpos > 250 && thisXpos <= zx) {
			adj = thisXpos - 250;
			if (adj > 0) {
				opp = adj * Math.tan(Math.PI/4);
				y = opp + 250;
				if (thisYpos > y) {
					console.log('changing y pos from ' + thisYpos + ' to ' + y);
					thisYpos = y;
				}
				if (thisYpos < 250) {
					console.log('changing y pos from ' + thisYpos + ' to ' + 250);
					thisYpos = 250;
				}
			}
		}
		if (started) {
			draw_context.lineTo(thisXpos, thisYpos);
			coords.push(new Cord(thisXpos, thisYpos));
			draw_context.lineTo(startx, starty);
			coords.push(new Cord(startx, starty));
			draw_context.closePath();
			draw_context.fill();
	
		    started = false;
		    coords.push(new Cord('X', 'X'));
		}
	});
	
	//fixes a problem where double clicking causes text to get selected on the canvas
	draw_canvas.addEventListener('selectstart', function(e) { e.preventDefault(); return false; }, false);
}

function clear_drawing_canvas()
{
	coords = new Array();
	draw_context.fillStyle = '#ffffff';
	draw_context.clearRect(0, 0, draw_canvas_width, draw_canvas_height);
	draw_canvas.width = draw_canvas.width;

	draw_context.fillStyle = 'white';
	draw_context.beginPath();
	draw_context.arc(250, 250, 240, 0, (Math.PI/4), false);
	draw_context.lineTo(250, 250);
	draw_context.closePath();
	draw_context.fill();
	draw_context.save();
}

function duplicate(coords) {
	// 45 degrees
	degree = Math.PI/4;
	draw_context.restore();
	// set rotation point
	draw_context.translate(250, 250);
	
	for (i = 1; i <= 7; i++) {
		draw_context.rotate(degree);
		draw_context.beginPath();
		draw_context.arc(0, 0, 240, 0, degree, false);
		draw_context.lineTo(0, 0);
		draw_context.closePath();
		draw_context.fill();
	}
	draw_context.restore();
	
	draw_context.scale(-1, 1);
	draw_context.rotate(3 * degree);
	copy_slice(draw_context, coords, 250, 250);
	draw_context.restore();
	
	draw_context.scale(-1, 1);
	draw_context.rotate(6 * degree);
	copy_slice(draw_context, coords, 250, 250);
	draw_context.restore();

	draw_context.scale(-1, 1);
	draw_context.rotate(8 * degree);
	copy_slice(draw_context, coords, 250, 250);
	draw_context.restore();

	draw_context.scale(-1, -1);
	draw_context.rotate(6 * degree);
	copy_slice(draw_context, coords, 250, 250);
	draw_context.restore();

	draw_context.scale(1, -1);
	draw_context.rotate(2 * degree);
	copy_slice(draw_context, coords, 250, 250);
	draw_context.restore();

	draw_context.scale(-1, -1);
	draw_context.rotate(2 * degree);
	copy_slice(draw_context, coords, 250, 250);
	draw_context.restore();
	
	draw_context.scale(-1, 1);
	draw_context.rotate(6 * degree);
	copy_slice(draw_context, coords, 250, 250);
	draw_context.restore();
}

function copy_slice(dctx, coords, addx, addy) {
	// first create copy of the slice
	dctx.globalCompositeOperation = "destination-out";

	if (coords.length > 0)
	{
		dctx.fillStyle = 'black';
		dctx.beginPath();
		dctx.moveTo(coords[0].x-addx, coords[0].y-addy);
		for ( i = 1; i < coords.length; i++ )
		{
			if (coords[i].y == 'X')
			{
				dctx.closePath();
				dctx.fill();
				console.log('resseting line');
				dctx.beginPath();
				if (typeof(coords[i+1]) != 'undefined')
				{
					dctx.moveTo( (coords[i+1].x-addx), (coords[i+1].y-addy) );
				}
			}
			else
			{
				dctx.lineTo((coords[i].x-addx), (coords[i].y-addy));
				console.log('drawing line to ' + (coords[i].x-addx) + 'x' + (coords[i].y-addy));
			}
		}
		dctx.closePath();
		dctx.fill();	
	}
	dctx.restore();
}


//$(window).resize(resizeCanvas);
//function resizeCanvas() {
//	$('canvas').attr("width", $(window).get(0).innerWidth);
//	$('canvas').attr("height", $(window).get(0).innerHeight);
//};
//resizeCanvas();