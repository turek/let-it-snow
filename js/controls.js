$(document).ready(function() {
	ctx = document.getElementById('canvas').getContext('2d');
	var canvasw = window.innerWidth;
	var canvash = window.innerHeight;
	ctx.canvas.width  = canvasw;
	ctx.canvas.height = canvash;

	// snowflakes generator
	for ( i = 0; i < fcount; i++ )
	{
		$.ajax({
			url: 'get_flake.php',
			success: function( data )
			{
				var thiscoords = jQuery.parseJSON( data );
				flakes.push( new Flake(i, 'sample text ' + i, ctx, fmaxr, thiscoords) );
				
//			    eval('thiscoords = ' + data + ';');
//				flakes.push( new Flake(i, 'sample text ' + i, ctx, fmaxr, thiscoords) );
			}
		});
	}

	// create stage
	stage = new Stage(flakes, ctx, fmaxr);
	stage.ctx.save();
	
	// animate snowflakes
	setInterval(function() {
		animation(stage);
	}, 1000/30); //30 frames per second
	

	prepare_drawing_canvas();
	clear_drawing_canvas();
	
	// bindings
	$('#canvas').mousemove(function(e) {
		mouseX = e.pageX;
		mouseY = e.pageY;
		$('#debug .one').text('X:' + mouseX + '; Y:' + mouseY);
	});
	
	$('#canvas').click(function(e) {
		if (matchedflake >= 0)
		{
			$('#details').show();
			$('#details').find('.title').text(flakes[matchedflake].text);
		}
		else
		{
			$('#details').hide();
		}
	});
	
	$('#show-btt').click(function() {
		duplicate(coords);
	});
	$('#redraw-btt').click(function() {
		clear_drawing_canvas();
	});
	
	$('#save-btt').click(function() {
		$.post("save_flake.php", { coords: coords } );
	});
});



//function animation (flakes,fcount,fmaxr,canvasw,canvash,ctx)
function animation (stage)
{
	//stage.ctx.restore();
//	stage.ctx.translate(0, 0);
//	stage.ctx.clearRect(0, 0, stage.width, stage.height);
//	background = stage.ctx.createLinearGradient(0, 0, 0, stage.height);
//	background.addColorStop(0, '#0D5995');
//	background.addColorStop(1, '#1096D5');
//	stage.ctx.fillStyle = background;
//	stage.ctx.fillRect(0, 0, this.width, this.height);
	//stage.ctx.save();

	stage.draw();
	stage.fall(15);
	stage.checkFlakesPosition(15);

	// for every flake check if it matches cursor
	for ( i = 0; i < stage.flakes.length; i++ )
	{
		thisx = stage.flakes[i].x; // x
		thisy = stage.flakes[i].y; // y
		thisr = stage.flakes[i].r+10; // r
		if (thisx-thisr < mouseX && thisx+thisr > mouseX && thisy-thisr < mouseY && thisy+thisr > mouseY)
		{
			matchedflake = i;
			$('#canvas').css('cursor','pointer');
			break;
		}
		else
		{
			matchedflake = -1;
			$('#canvas').css('cursor','auto');
		}
	}
}
