-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Czas wygenerowania: 29 Gru 2011, 08:45
-- Wersja serwera: 5.5.8
-- Wersja PHP: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Baza danych: `flakes`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `flakes`
--

CREATE TABLE IF NOT EXISTS `flakes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `title` varchar(150) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `addr` varchar(15) DEFAULT NULL,
  `datetime` datetime NOT NULL,
  `coords` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `datetime` (`datetime`),
  KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Zrzut danych tabeli `flakes`
--

INSERT INTO `flakes` VALUES(1, 'test', 'test test', '', '127.0.0.1', '2011-12-27 15:42:59', NULL);
INSERT INTO `flakes` VALUES(2, 'test', 'test test', '', '127.0.0.1', '2011-12-27 16:11:39', NULL);
INSERT INTO `flakes` VALUES(3, 'test', 'test test', '', '127.0.0.1', '2011-12-27 16:17:53', NULL);
INSERT INTO `flakes` VALUES(4, 'test', 'test test', '', '127.0.0.1', '2011-12-27 19:01:57', NULL);
INSERT INTO `flakes` VALUES(5, 'test', 'test test', '', '127.0.0.1', '2011-12-27 19:02:37', NULL);
INSERT INTO `flakes` VALUES(6, 'test', 'test test', '', '127.0.0.1', '2011-12-28 00:07:06', NULL);
INSERT INTO `flakes` VALUES(7, 'test', 'test test', '', '127.0.0.1', '2011-12-28 00:08:08', NULL);
INSERT INTO `flakes` VALUES(8, 'test', 'test test', '', '127.0.0.1', '2011-12-28 11:27:21', NULL);
INSERT INTO `flakes` VALUES(9, 'test', 'test test', '', '127.0.0.1', '2011-12-28 21:19:51', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `flake_coords`
--

CREATE TABLE IF NOT EXISTS `flake_coords` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `flake_id` int(11) NOT NULL DEFAULT '0',
  `x` varchar(3) NOT NULL DEFAULT 'X',
  `y` varchar(3) NOT NULL DEFAULT 'X',
  `order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `flake_id` (`flake_id`),
  KEY `order` (`order`),
  KEY `y` (`y`),
  KEY `x` (`x`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=400 ;

--
-- Zrzut danych tabeli `flake_coords`
--

INSERT INTO `flake_coords` VALUES(1, 1, '258', '258', 0);
INSERT INTO `flake_coords` VALUES(2, 1, '337', '270', 1);
INSERT INTO `flake_coords` VALUES(3, 1, '333', '281', 2);
INSERT INTO `flake_coords` VALUES(4, 1, '396', '265', 3);
INSERT INTO `flake_coords` VALUES(5, 1, '388', '293', 4);
INSERT INTO `flake_coords` VALUES(6, 1, '458', '278', 5);
INSERT INTO `flake_coords` VALUES(7, 1, '431', '319', 6);
INSERT INTO `flake_coords` VALUES(8, 1, '408', '379', 7);
INSERT INTO `flake_coords` VALUES(9, 1, '384', '315', 8);
INSERT INTO `flake_coords` VALUES(10, 1, '365', '339', 9);
INSERT INTO `flake_coords` VALUES(11, 1, '335', '294', 10);
INSERT INTO `flake_coords` VALUES(12, 1, '322', '322', 11);
INSERT INTO `flake_coords` VALUES(13, 1, '322', '322', 12);
INSERT INTO `flake_coords` VALUES(14, 1, '322', '322', 13);
INSERT INTO `flake_coords` VALUES(15, 1, '258', '258', 14);
INSERT INTO `flake_coords` VALUES(16, 1, 'X', 'X', 15);
INSERT INTO `flake_coords` VALUES(17, 1, '333', '333', 16);
INSERT INTO `flake_coords` VALUES(18, 1, '339', '314', 17);
INSERT INTO `flake_coords` VALUES(19, 1, '367', '359', 18);
INSERT INTO `flake_coords` VALUES(20, 1, '383', '338', 19);
INSERT INTO `flake_coords` VALUES(21, 1, '419', '419', 20);
INSERT INTO `flake_coords` VALUES(22, 1, '419', '419', 21);
INSERT INTO `flake_coords` VALUES(23, 1, '419', '419', 22);
INSERT INTO `flake_coords` VALUES(24, 1, '333', '333', 23);
INSERT INTO `flake_coords` VALUES(25, 1, 'X', 'X', 24);
INSERT INTO `flake_coords` VALUES(26, 1, '418', '383', 25);
INSERT INTO `flake_coords` VALUES(27, 1, '442', '326', 26);
INSERT INTO `flake_coords` VALUES(28, 1, '499', '276', 27);
INSERT INTO `flake_coords` VALUES(29, 1, '459', '327', 28);
INSERT INTO `flake_coords` VALUES(30, 1, '448', '356', 29);
INSERT INTO `flake_coords` VALUES(31, 1, '458', '386', 30);
INSERT INTO `flake_coords` VALUES(32, 1, '422', '391', 31);
INSERT INTO `flake_coords` VALUES(33, 1, '422', '391', 32);
INSERT INTO `flake_coords` VALUES(34, 1, '422', '391', 33);
INSERT INTO `flake_coords` VALUES(35, 1, '418', '383', 34);
INSERT INTO `flake_coords` VALUES(36, 1, 'X', 'X', 35);
INSERT INTO `flake_coords` VALUES(37, 1, '441', '400', 36);
INSERT INTO `flake_coords` VALUES(38, 1, '425', '398', 37);
INSERT INTO `flake_coords` VALUES(39, 1, '430', '413', 38);
INSERT INTO `flake_coords` VALUES(40, 1, '430', '413', 39);
INSERT INTO `flake_coords` VALUES(41, 1, '430', '413', 40);
INSERT INTO `flake_coords` VALUES(42, 1, '441', '400', 41);
INSERT INTO `flake_coords` VALUES(43, 1, 'X', 'X', 42);
INSERT INTO `flake_coords` VALUES(44, 1, '494', '272', 43);
INSERT INTO `flake_coords` VALUES(45, 1, '448', '312', 44);
INSERT INTO `flake_coords` VALUES(46, 1, '479', '266', 45);
INSERT INTO `flake_coords` VALUES(47, 1, '409', '275', 46);
INSERT INTO `flake_coords` VALUES(48, 1, '417', '250', 47);
INSERT INTO `flake_coords` VALUES(49, 1, '498', '239', 48);
INSERT INTO `flake_coords` VALUES(50, 1, '498', '239', 49);
INSERT INTO `flake_coords` VALUES(51, 1, '498', '239', 50);
INSERT INTO `flake_coords` VALUES(52, 1, '494', '272', 51);
INSERT INTO `flake_coords` VALUES(53, 1, 'X', 'X', 52);
INSERT INTO `flake_coords` VALUES(54, 2, '270', '250', 0);
INSERT INTO `flake_coords` VALUES(55, 2, '279', '273', 1);
INSERT INTO `flake_coords` VALUES(56, 2, '290', '265', 2);
INSERT INTO `flake_coords` VALUES(57, 2, '305', '292', 3);
INSERT INTO `flake_coords` VALUES(58, 2, '316', '263', 4);
INSERT INTO `flake_coords` VALUES(59, 2, '311', '303', 5);
INSERT INTO `flake_coords` VALUES(60, 2, '346', '274', 6);
INSERT INTO `flake_coords` VALUES(61, 2, '362', '325', 7);
INSERT INTO `flake_coords` VALUES(62, 2, '384', '250', 8);
INSERT INTO `flake_coords` VALUES(63, 2, '384', '250', 9);
INSERT INTO `flake_coords` VALUES(64, 2, '384', '250', 10);
INSERT INTO `flake_coords` VALUES(65, 2, '384', '250', 11);
INSERT INTO `flake_coords` VALUES(66, 2, '270', '250', 12);
INSERT INTO `flake_coords` VALUES(67, 2, 'X', 'X', 13);
INSERT INTO `flake_coords` VALUES(68, 2, '397', '250', 14);
INSERT INTO `flake_coords` VALUES(69, 2, '396', '345', 15);
INSERT INTO `flake_coords` VALUES(70, 2, '411', '297', 16);
INSERT INTO `flake_coords` VALUES(71, 2, '420', '340', 17);
INSERT INTO `flake_coords` VALUES(72, 2, '435', '301', 18);
INSERT INTO `flake_coords` VALUES(73, 2, '436', '353', 19);
INSERT INTO `flake_coords` VALUES(74, 2, '465', '311', 20);
INSERT INTO `flake_coords` VALUES(75, 2, '462', '342', 21);
INSERT INTO `flake_coords` VALUES(76, 2, '484', '349', 22);
INSERT INTO `flake_coords` VALUES(77, 2, '484', '349', 23);
INSERT INTO `flake_coords` VALUES(78, 2, '484', '349', 24);
INSERT INTO `flake_coords` VALUES(79, 2, '397', '250', 25);
INSERT INTO `flake_coords` VALUES(80, 2, 'X', 'X', 26);
INSERT INTO `flake_coords` VALUES(81, 2, '437', '245', 27);
INSERT INTO `flake_coords` VALUES(82, 2, '486', '329', 28);
INSERT INTO `flake_coords` VALUES(83, 2, '498', '247', 29);
INSERT INTO `flake_coords` VALUES(84, 2, '496', '243', 30);
INSERT INTO `flake_coords` VALUES(85, 2, '496', '243', 31);
INSERT INTO `flake_coords` VALUES(86, 2, '437', '245', 32);
INSERT INTO `flake_coords` VALUES(87, 2, 'X', 'X', 33);
INSERT INTO `flake_coords` VALUES(88, 2, '484', '358', 34);
INSERT INTO `flake_coords` VALUES(89, 2, '339', '339', 35);
INSERT INTO `flake_coords` VALUES(90, 2, '423', '445', 36);
INSERT INTO `flake_coords` VALUES(91, 2, '423', '445', 37);
INSERT INTO `flake_coords` VALUES(92, 2, '423', '445', 38);
INSERT INTO `flake_coords` VALUES(93, 2, '484', '358', 39);
INSERT INTO `flake_coords` VALUES(94, 2, 'X', 'X', 40);
INSERT INTO `flake_coords` VALUES(95, 3, '413', '413', 0);
INSERT INTO `flake_coords` VALUES(96, 3, '401', '369', 1);
INSERT INTO `flake_coords` VALUES(97, 3, '427', '387', 2);
INSERT INTO `flake_coords` VALUES(98, 3, '411', '333', 3);
INSERT INTO `flake_coords` VALUES(99, 3, '451', '349', 4);
INSERT INTO `flake_coords` VALUES(100, 3, '415', '304', 5);
INSERT INTO `flake_coords` VALUES(101, 3, '469', '313', 6);
INSERT INTO `flake_coords` VALUES(102, 3, '435', '274', 7);
INSERT INTO `flake_coords` VALUES(103, 3, '465', '272', 8);
INSERT INTO `flake_coords` VALUES(104, 3, '446', '230', 9);
INSERT INTO `flake_coords` VALUES(105, 3, '499', '251', 10);
INSERT INTO `flake_coords` VALUES(106, 3, '456', '462', 11);
INSERT INTO `flake_coords` VALUES(107, 3, '456', '462', 12);
INSERT INTO `flake_coords` VALUES(108, 3, '456', '462', 13);
INSERT INTO `flake_coords` VALUES(109, 3, '413', '413', 14);
INSERT INTO `flake_coords` VALUES(110, 3, 'X', 'X', 15);
INSERT INTO `flake_coords` VALUES(111, 3, '306', '287', 16);
INSERT INTO `flake_coords` VALUES(112, 3, '373', '357', 17);
INSERT INTO `flake_coords` VALUES(113, 3, '394', '310', 18);
INSERT INTO `flake_coords` VALUES(114, 3, '350', '299', 19);
INSERT INTO `flake_coords` VALUES(115, 3, '418', '285', 20);
INSERT INTO `flake_coords` VALUES(116, 3, '310', '289', 21);
INSERT INTO `flake_coords` VALUES(117, 3, '310', '289', 22);
INSERT INTO `flake_coords` VALUES(118, 3, '310', '289', 23);
INSERT INTO `flake_coords` VALUES(119, 3, '306', '287', 24);
INSERT INTO `flake_coords` VALUES(120, 3, 'X', 'X', 25);
INSERT INTO `flake_coords` VALUES(121, 3, '263', '250', 26);
INSERT INTO `flake_coords` VALUES(122, 3, '294', '279', 27);
INSERT INTO `flake_coords` VALUES(123, 3, '418', '274', 28);
INSERT INTO `flake_coords` VALUES(124, 3, '417', '264', 29);
INSERT INTO `flake_coords` VALUES(125, 3, '446', '262', 30);
INSERT INTO `flake_coords` VALUES(126, 3, '437', '239', 31);
INSERT INTO `flake_coords` VALUES(127, 3, '437', '239', 32);
INSERT INTO `flake_coords` VALUES(128, 3, '437', '239', 33);
INSERT INTO `flake_coords` VALUES(129, 3, '263', '250', 34);
INSERT INTO `flake_coords` VALUES(130, 3, 'X', 'X', 35);
INSERT INTO `flake_coords` VALUES(131, 4, '247', '259', 0);
INSERT INTO `flake_coords` VALUES(132, 4, '384', '266', 1);
INSERT INTO `flake_coords` VALUES(133, 4, '384', '280', 2);
INSERT INTO `flake_coords` VALUES(134, 4, '387', '298', 3);
INSERT INTO `flake_coords` VALUES(135, 4, '385', '314', 4);
INSERT INTO `flake_coords` VALUES(136, 4, '385', '314', 5);
INSERT INTO `flake_coords` VALUES(137, 4, '247', '259', 6);
INSERT INTO `flake_coords` VALUES(138, 4, 'X', 'X', 7);
INSERT INTO `flake_coords` VALUES(139, 4, '383', '325', 8);
INSERT INTO `flake_coords` VALUES(140, 4, '381', '330', 9);
INSERT INTO `flake_coords` VALUES(141, 4, '381', '330', 10);
INSERT INTO `flake_coords` VALUES(142, 4, '383', '325', 11);
INSERT INTO `flake_coords` VALUES(143, 4, 'X', 'X', 12);
INSERT INTO `flake_coords` VALUES(144, 4, '375', '341', 13);
INSERT INTO `flake_coords` VALUES(145, 4, '372', '347', 14);
INSERT INTO `flake_coords` VALUES(146, 4, '372', '347', 15);
INSERT INTO `flake_coords` VALUES(147, 4, '375', '341', 16);
INSERT INTO `flake_coords` VALUES(148, 4, 'X', 'X', 17);
INSERT INTO `flake_coords` VALUES(149, 4, '367', '352', 18);
INSERT INTO `flake_coords` VALUES(150, 4, '332', '312', 19);
INSERT INTO `flake_coords` VALUES(151, 4, '332', '312', 20);
INSERT INTO `flake_coords` VALUES(152, 4, '332', '312', 21);
INSERT INTO `flake_coords` VALUES(153, 4, '367', '352', 22);
INSERT INTO `flake_coords` VALUES(154, 4, 'X', 'X', 23);
INSERT INTO `flake_coords` VALUES(155, 4, '389', '322', 24);
INSERT INTO `flake_coords` VALUES(156, 4, '384', '356', 25);
INSERT INTO `flake_coords` VALUES(157, 4, '384', '356', 26);
INSERT INTO `flake_coords` VALUES(158, 4, '384', '356', 27);
INSERT INTO `flake_coords` VALUES(159, 4, '389', '322', 28);
INSERT INTO `flake_coords` VALUES(160, 4, 'X', 'X', 29);
INSERT INTO `flake_coords` VALUES(161, 4, '369', '352', 30);
INSERT INTO `flake_coords` VALUES(162, 4, '384', '366', 31);
INSERT INTO `flake_coords` VALUES(163, 4, '408', '323', 32);
INSERT INTO `flake_coords` VALUES(164, 4, '324', '313', 33);
INSERT INTO `flake_coords` VALUES(165, 4, '324', '313', 34);
INSERT INTO `flake_coords` VALUES(166, 4, '324', '313', 35);
INSERT INTO `flake_coords` VALUES(167, 4, '369', '352', 36);
INSERT INTO `flake_coords` VALUES(168, 4, 'X', 'X', 37);
INSERT INTO `flake_coords` VALUES(169, 4, '408', '264', 38);
INSERT INTO `flake_coords` VALUES(170, 4, '419', '347', 39);
INSERT INTO `flake_coords` VALUES(171, 4, '459', '308', 40);
INSERT INTO `flake_coords` VALUES(172, 4, '407', '261', 41);
INSERT INTO `flake_coords` VALUES(173, 4, '407', '261', 42);
INSERT INTO `flake_coords` VALUES(174, 4, '407', '261', 43);
INSERT INTO `flake_coords` VALUES(175, 4, '408', '264', 44);
INSERT INTO `flake_coords` VALUES(176, 4, 'X', 'X', 45);
INSERT INTO `flake_coords` VALUES(177, 4, '434', '259', 46);
INSERT INTO `flake_coords` VALUES(178, 4, '474', '321', 47);
INSERT INTO `flake_coords` VALUES(179, 4, '487', '353', 48);
INSERT INTO `flake_coords` VALUES(180, 4, '499', '244', 49);
INSERT INTO `flake_coords` VALUES(181, 4, '420', '234', 50);
INSERT INTO `flake_coords` VALUES(182, 4, '420', '234', 51);
INSERT INTO `flake_coords` VALUES(183, 4, '420', '234', 52);
INSERT INTO `flake_coords` VALUES(184, 4, '434', '259', 53);
INSERT INTO `flake_coords` VALUES(185, 4, 'X', 'X', 54);
INSERT INTO `flake_coords` VALUES(186, 4, '483', '313', 55);
INSERT INTO `flake_coords` VALUES(187, 4, '366', '366', 56);
INSERT INTO `flake_coords` VALUES(188, 4, '443', '459', 57);
INSERT INTO `flake_coords` VALUES(189, 4, '443', '459', 58);
INSERT INTO `flake_coords` VALUES(190, 4, '443', '459', 59);
INSERT INTO `flake_coords` VALUES(191, 4, '483', '313', 60);
INSERT INTO `flake_coords` VALUES(192, 4, 'X', 'X', 61);
INSERT INTO `flake_coords` VALUES(193, 5, '263', '250', 0);
INSERT INTO `flake_coords` VALUES(194, 5, '289', '289', 1);
INSERT INTO `flake_coords` VALUES(195, 5, '203', '237', 2);
INSERT INTO `flake_coords` VALUES(196, 5, '203', '237', 3);
INSERT INTO `flake_coords` VALUES(197, 5, '203', '237', 4);
INSERT INTO `flake_coords` VALUES(198, 5, '263', '250', 5);
INSERT INTO `flake_coords` VALUES(199, 5, 'X', 'X', 6);
INSERT INTO `flake_coords` VALUES(200, 5, '311', '250', 7);
INSERT INTO `flake_coords` VALUES(201, 5, '318', '262', 8);
INSERT INTO `flake_coords` VALUES(202, 5, '316', '276', 9);
INSERT INTO `flake_coords` VALUES(203, 5, '309', '290', 10);
INSERT INTO `flake_coords` VALUES(204, 5, '301', '301', 11);
INSERT INTO `flake_coords` VALUES(205, 5, '219', '235', 12);
INSERT INTO `flake_coords` VALUES(206, 5, '219', '235', 13);
INSERT INTO `flake_coords` VALUES(207, 5, '219', '235', 14);
INSERT INTO `flake_coords` VALUES(208, 5, '311', '250', 15);
INSERT INTO `flake_coords` VALUES(209, 5, 'X', 'X', 16);
INSERT INTO `flake_coords` VALUES(210, 5, '311', '311', 17);
INSERT INTO `flake_coords` VALUES(211, 5, '323', '295', 18);
INSERT INTO `flake_coords` VALUES(212, 5, '336', '269', 19);
INSERT INTO `flake_coords` VALUES(213, 5, '430', '271', 20);
INSERT INTO `flake_coords` VALUES(214, 5, '425', '325', 21);
INSERT INTO `flake_coords` VALUES(215, 5, '410', '350', 22);
INSERT INTO `flake_coords` VALUES(216, 5, '385', '381', 23);
INSERT INTO `flake_coords` VALUES(217, 5, '354', '354', 24);
INSERT INTO `flake_coords` VALUES(218, 5, '354', '354', 25);
INSERT INTO `flake_coords` VALUES(219, 5, '354', '354', 26);
INSERT INTO `flake_coords` VALUES(220, 5, '311', '311', 27);
INSERT INTO `flake_coords` VALUES(221, 5, 'X', 'X', 28);
INSERT INTO `flake_coords` VALUES(222, 5, '430', '430', 29);
INSERT INTO `flake_coords` VALUES(223, 5, '419', '387', 30);
INSERT INTO `flake_coords` VALUES(224, 5, '437', '372', 31);
INSERT INTO `flake_coords` VALUES(225, 5, '439', '344', 32);
INSERT INTO `flake_coords` VALUES(226, 5, '455', '337', 33);
INSERT INTO `flake_coords` VALUES(227, 5, '464', '313', 34);
INSERT INTO `flake_coords` VALUES(228, 5, '441', '287', 35);
INSERT INTO `flake_coords` VALUES(229, 5, '449', '232', 36);
INSERT INTO `flake_coords` VALUES(230, 5, '495', '244', 37);
INSERT INTO `flake_coords` VALUES(231, 5, '446', '471', 38);
INSERT INTO `flake_coords` VALUES(232, 5, '446', '471', 39);
INSERT INTO `flake_coords` VALUES(233, 5, '446', '471', 40);
INSERT INTO `flake_coords` VALUES(234, 5, '430', '430', 41);
INSERT INTO `flake_coords` VALUES(235, 5, 'X', 'X', 42);
INSERT INTO `flake_coords` VALUES(236, 5, '483', '346', 43);
INSERT INTO `flake_coords` VALUES(237, 5, '472', '330', 44);
INSERT INTO `flake_coords` VALUES(238, 5, '485', '253', 45);
INSERT INTO `flake_coords` VALUES(239, 5, '494', '268', 46);
INSERT INTO `flake_coords` VALUES(240, 5, '489', '346', 47);
INSERT INTO `flake_coords` VALUES(241, 5, '489', '346', 48);
INSERT INTO `flake_coords` VALUES(242, 5, '489', '346', 49);
INSERT INTO `flake_coords` VALUES(243, 5, '483', '346', 50);
INSERT INTO `flake_coords` VALUES(244, 5, 'X', 'X', 51);
INSERT INTO `flake_coords` VALUES(245, 6, '416', '250', 0);
INSERT INTO `flake_coords` VALUES(246, 6, '416', '250', 1);
INSERT INTO `flake_coords` VALUES(247, 6, '416', '250', 2);
INSERT INTO `flake_coords` VALUES(248, 6, '416', '250', 3);
INSERT INTO `flake_coords` VALUES(249, 6, 'X', 'X', 4);
INSERT INTO `flake_coords` VALUES(250, 6, '416', '250', 5);
INSERT INTO `flake_coords` VALUES(251, 6, '416', '250', 6);
INSERT INTO `flake_coords` VALUES(252, 6, '362', '250', 7);
INSERT INTO `flake_coords` VALUES(253, 6, '357', '250', 8);
INSERT INTO `flake_coords` VALUES(254, 6, '357', '250', 9);
INSERT INTO `flake_coords` VALUES(255, 6, '416', '250', 10);
INSERT INTO `flake_coords` VALUES(256, 6, 'X', 'X', 11);
INSERT INTO `flake_coords` VALUES(257, 6, '291', '291', 12);
INSERT INTO `flake_coords` VALUES(258, 6, '324', '273', 13);
INSERT INTO `flake_coords` VALUES(259, 6, '323', '310', 14);
INSERT INTO `flake_coords` VALUES(260, 6, '383', '279', 15);
INSERT INTO `flake_coords` VALUES(261, 6, '384', '339', 16);
INSERT INTO `flake_coords` VALUES(262, 6, '423', '314', 17);
INSERT INTO `flake_coords` VALUES(263, 6, '365', '365', 18);
INSERT INTO `flake_coords` VALUES(264, 6, '322', '322', 19);
INSERT INTO `flake_coords` VALUES(265, 6, '322', '322', 20);
INSERT INTO `flake_coords` VALUES(266, 6, '322', '322', 21);
INSERT INTO `flake_coords` VALUES(267, 6, '291', '291', 22);
INSERT INTO `flake_coords` VALUES(268, 6, 'X', 'X', 23);
INSERT INTO `flake_coords` VALUES(269, 6, '322', '322', 24);
INSERT INTO `flake_coords` VALUES(270, 6, '455', '237', 25);
INSERT INTO `flake_coords` VALUES(271, 6, '455', '237', 26);
INSERT INTO `flake_coords` VALUES(272, 6, '455', '237', 27);
INSERT INTO `flake_coords` VALUES(273, 6, '322', '322', 28);
INSERT INTO `flake_coords` VALUES(274, 6, 'X', 'X', 29);
INSERT INTO `flake_coords` VALUES(275, 6, '454', '262', 30);
INSERT INTO `flake_coords` VALUES(276, 6, '451', '323', 31);
INSERT INTO `flake_coords` VALUES(277, 6, '411', '279', 32);
INSERT INTO `flake_coords` VALUES(278, 6, '396', '250', 33);
INSERT INTO `flake_coords` VALUES(279, 6, '454', '217', 34);
INSERT INTO `flake_coords` VALUES(280, 6, '454', '217', 35);
INSERT INTO `flake_coords` VALUES(281, 6, '454', '217', 36);
INSERT INTO `flake_coords` VALUES(282, 6, '454', '262', 37);
INSERT INTO `flake_coords` VALUES(283, 6, 'X', 'X', 38);
INSERT INTO `flake_coords` VALUES(284, 6, '449', '312', 39);
INSERT INTO `flake_coords` VALUES(285, 6, '473', '402', 40);
INSERT INTO `flake_coords` VALUES(286, 6, '477', '274', 41);
INSERT INTO `flake_coords` VALUES(287, 6, '471', '222', 42);
INSERT INTO `flake_coords` VALUES(288, 6, '471', '222', 43);
INSERT INTO `flake_coords` VALUES(289, 6, '471', '222', 44);
INSERT INTO `flake_coords` VALUES(290, 6, '449', '312', 45);
INSERT INTO `flake_coords` VALUES(291, 6, 'X', 'X', 46);
INSERT INTO `flake_coords` VALUES(292, 6, '457', '373', 47);
INSERT INTO `flake_coords` VALUES(293, 6, '433', '331', 48);
INSERT INTO `flake_coords` VALUES(294, 6, '426', '378', 49);
INSERT INTO `flake_coords` VALUES(295, 6, '428', '326', 50);
INSERT INTO `flake_coords` VALUES(296, 6, '403', '375', 51);
INSERT INTO `flake_coords` VALUES(297, 6, '403', '339', 52);
INSERT INTO `flake_coords` VALUES(298, 6, '382', '382', 53);
INSERT INTO `flake_coords` VALUES(299, 6, '411', '370', 54);
INSERT INTO `flake_coords` VALUES(300, 6, '411', '370', 55);
INSERT INTO `flake_coords` VALUES(301, 6, '411', '370', 56);
INSERT INTO `flake_coords` VALUES(302, 6, '457', '373', 57);
INSERT INTO `flake_coords` VALUES(303, 6, 'X', 'X', 58);
INSERT INTO `flake_coords` VALUES(304, 6, '413', '305', 59);
INSERT INTO `flake_coords` VALUES(305, 6, '389', '261', 60);
INSERT INTO `flake_coords` VALUES(306, 6, '427', '299', 61);
INSERT INTO `flake_coords` VALUES(307, 6, '421', '254', 62);
INSERT INTO `flake_coords` VALUES(308, 6, '409', '252', 63);
INSERT INTO `flake_coords` VALUES(309, 6, '408', '253', 64);
INSERT INTO `flake_coords` VALUES(310, 6, '408', '253', 65);
INSERT INTO `flake_coords` VALUES(311, 6, '413', '305', 66);
INSERT INTO `flake_coords` VALUES(312, 6, 'X', 'X', 67);
INSERT INTO `flake_coords` VALUES(313, 6, '385', '250', 68);
INSERT INTO `flake_coords` VALUES(314, 6, '353', '274', 69);
INSERT INTO `flake_coords` VALUES(315, 6, '348', '251', 70);
INSERT INTO `flake_coords` VALUES(316, 6, '335', '265', 71);
INSERT INTO `flake_coords` VALUES(317, 6, '315', '253', 72);
INSERT INTO `flake_coords` VALUES(318, 6, '296', '265', 73);
INSERT INTO `flake_coords` VALUES(319, 6, '303', '250', 74);
INSERT INTO `flake_coords` VALUES(320, 6, '338', '250', 75);
INSERT INTO `flake_coords` VALUES(321, 6, '338', '250', 76);
INSERT INTO `flake_coords` VALUES(322, 6, '338', '250', 77);
INSERT INTO `flake_coords` VALUES(323, 6, '385', '250', 78);
INSERT INTO `flake_coords` VALUES(324, 6, 'X', 'X', 79);
INSERT INTO `flake_coords` VALUES(325, 7, '250', '253', 0);
INSERT INTO `flake_coords` VALUES(326, 7, '486', '257', 1);
INSERT INTO `flake_coords` VALUES(327, 7, '267', '264', 2);
INSERT INTO `flake_coords` VALUES(328, 7, '486', '274', 3);
INSERT INTO `flake_coords` VALUES(329, 7, '277', '273', 4);
INSERT INTO `flake_coords` VALUES(330, 7, '481', '290', 5);
INSERT INTO `flake_coords` VALUES(331, 7, '292', '287', 6);
INSERT INTO `flake_coords` VALUES(332, 7, '474', '302', 7);
INSERT INTO `flake_coords` VALUES(333, 7, '308', '297', 8);
INSERT INTO `flake_coords` VALUES(334, 7, '467', '323', 9);
INSERT INTO `flake_coords` VALUES(335, 7, '328', '313', 10);
INSERT INTO `flake_coords` VALUES(336, 7, '463', '334', 11);
INSERT INTO `flake_coords` VALUES(337, 7, '322', '322', 12);
INSERT INTO `flake_coords` VALUES(338, 7, '270', '270', 13);
INSERT INTO `flake_coords` VALUES(339, 7, '269', '269', 14);
INSERT INTO `flake_coords` VALUES(340, 7, '269', '269', 15);
INSERT INTO `flake_coords` VALUES(341, 7, '250', '253', 16);
INSERT INTO `flake_coords` VALUES(342, 7, 'X', 'X', 17);
INSERT INTO `flake_coords` VALUES(343, 7, '465', '346', 18);
INSERT INTO `flake_coords` VALUES(344, 7, '397', '340', 19);
INSERT INTO `flake_coords` VALUES(345, 7, '422', '364', 20);
INSERT INTO `flake_coords` VALUES(346, 7, '392', '356', 21);
INSERT INTO `flake_coords` VALUES(347, 7, '405', '375', 22);
INSERT INTO `flake_coords` VALUES(348, 7, '343', '343', 23);
INSERT INTO `flake_coords` VALUES(349, 7, '421', '445', 24);
INSERT INTO `flake_coords` VALUES(350, 7, '483', '388', 25);
INSERT INTO `flake_coords` VALUES(351, 7, '483', '388', 26);
INSERT INTO `flake_coords` VALUES(352, 7, '483', '388', 27);
INSERT INTO `flake_coords` VALUES(353, 7, '465', '346', 28);
INSERT INTO `flake_coords` VALUES(354, 7, 'X', 'X', 29);
INSERT INTO `flake_coords` VALUES(355, 7, '456', '351', 30);
INSERT INTO `flake_coords` VALUES(356, 7, '490', '323', 31);
INSERT INTO `flake_coords` VALUES(357, 7, '475', '362', 32);
INSERT INTO `flake_coords` VALUES(358, 7, '475', '362', 33);
INSERT INTO `flake_coords` VALUES(359, 7, '475', '362', 34);
INSERT INTO `flake_coords` VALUES(360, 7, '456', '351', 35);
INSERT INTO `flake_coords` VALUES(361, 7, 'X', 'X', 36);
INSERT INTO `flake_coords` VALUES(362, 8, '329', '260', 0);
INSERT INTO `flake_coords` VALUES(363, 8, '349', '319', 1);
INSERT INTO `flake_coords` VALUES(364, 8, '411', '270', 2);
INSERT INTO `flake_coords` VALUES(365, 8, '387', '372', 3);
INSERT INTO `flake_coords` VALUES(366, 8, '446', '293', 4);
INSERT INTO `flake_coords` VALUES(367, 8, '444', '363', 5);
INSERT INTO `flake_coords` VALUES(368, 8, '471', '251', 6);
INSERT INTO `flake_coords` VALUES(369, 8, '471', '251', 7);
INSERT INTO `flake_coords` VALUES(370, 8, '471', '251', 8);
INSERT INTO `flake_coords` VALUES(371, 8, '329', '260', 9);
INSERT INTO `flake_coords` VALUES(372, 8, 'X', 'X', 10);
INSERT INTO `flake_coords` VALUES(373, 8, '381', '381', 11);
INSERT INTO `flake_coords` VALUES(374, 8, '433', '329', 12);
INSERT INTO `flake_coords` VALUES(375, 8, '447', '411', 13);
INSERT INTO `flake_coords` VALUES(376, 8, '425', '469', 14);
INSERT INTO `flake_coords` VALUES(377, 8, '425', '469', 15);
INSERT INTO `flake_coords` VALUES(378, 8, '425', '469', 16);
INSERT INTO `flake_coords` VALUES(379, 8, '381', '381', 17);
INSERT INTO `flake_coords` VALUES(380, 8, 'X', 'X', 18);
INSERT INTO `flake_coords` VALUES(381, 8, '307', '250', 19);
INSERT INTO `flake_coords` VALUES(382, 8, '352', '352', 20);
INSERT INTO `flake_coords` VALUES(383, 8, '248', '250', 21);
INSERT INTO `flake_coords` VALUES(384, 8, '248', '250', 22);
INSERT INTO `flake_coords` VALUES(385, 8, '248', '250', 23);
INSERT INTO `flake_coords` VALUES(386, 8, '307', '250', 24);
INSERT INTO `flake_coords` VALUES(387, 8, 'X', 'X', 25);
INSERT INTO `flake_coords` VALUES(388, 9, '243', '259', 0);
INSERT INTO `flake_coords` VALUES(389, 9, '370', '275', 1);
INSERT INTO `flake_coords` VALUES(390, 9, '425', '270', 2);
INSERT INTO `flake_coords` VALUES(391, 9, '426', '295', 3);
INSERT INTO `flake_coords` VALUES(392, 9, '426', '320', 4);
INSERT INTO `flake_coords` VALUES(393, 9, '369', '364', 5);
INSERT INTO `flake_coords` VALUES(394, 9, '344', '344', 6);
INSERT INTO `flake_coords` VALUES(395, 9, '344', '344', 7);
INSERT INTO `flake_coords` VALUES(396, 9, '344', '344', 8);
INSERT INTO `flake_coords` VALUES(397, 9, '344', '344', 9);
INSERT INTO `flake_coords` VALUES(398, 9, '243', '259', 10);
INSERT INTO `flake_coords` VALUES(399, 9, 'X', 'X', 11);
