<?php
$base_path = '/snowflakes/';
?><!DOCTYPE html>
<html>
<head>
	<title>Snow</title>
	<meta charset="utf-8">
<?php
$url = explode('/', $_SERVER['REQUEST_URI']);
$id = (int)trim(array_pop($url));
if (is_int($id) && $id > 0 && array_pop($url) == 'flake')
{
	?>
	<meta property="og:type" content="activity"/>
	<meta property="og:url" content="http://www.letitsnow.me/flake/<?php print $id; ?>"/>
	<meta property="og:image" content="http://www.thesinglelanesuperhighway.com/files/carThumbs/icon-draw-your-car1.jpg"/>
	<meta property="og:description" content="The Single Lane Superhighway. Watch the cars drive by. Where are they going? The journey is yours."/>	<link href="<?php print $base_path; ?>styles/style.css" rel="stylesheet" type="text/css" />
	<?php
}
else
{
	?>
	<meta property="og:type" content="activity"/>
	<meta property="og:url" content="http://www.letitsnow.me/"/>
	<meta property="og:image" content="http://www.letitsnow.me/images/snowflake.png"/>
	<meta property="og:description" content="The Single Lane Superhighway. Watch the cars drive by. Where are they going? The journey is yours."/>	<link href="<?php print $base_path; ?>styles/style.css" rel="stylesheet" type="text/css" />
	<?php
}
?>
	<script src="<?php print $base_path; ?>js/jquery.min.js" type="text/javascript"></script>
	<script type="text/javascript">var base_url = '<?php print $base_path; ?>';</script>
</head>

<body>
	
<canvas id="snow" width="800" height="800">
	Canvas not supported
</canvas>

<div id="fb-root"><!-- --></div>
<script src="<?php print $base_path; ?>js/script.js" type="text/javascript"></script>

</body>
</html>