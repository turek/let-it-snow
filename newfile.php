Please consider such code, draw() function is called in each animation loop, where stage is cleared and gradient is applied to a background and x,y are changed for shapes:

<pre>
<code>
draw = function() {
	for ( i = 0; i < shapes.length; i++ )
	{
		draw_sample_shape(i);
		draw_coords(i);
	}
}

draw_sample_shape = function(shape) {
	ctx.globalCompositeOperation = "source-over";
	ctx.fillStyle = color;
	ctx.beginPath();
	ctx.arc(shapes[shape].x, shapes[shape].y, 240, 0, Math.PI*2, false);
	ctx.closePath();
	ctx.fill();
}

draw_coords(shape) {
	coords = shapes[shape].coords;
	ctx.globalCompositeOperation = "destination-out";
	ctx.beginPath();
	ctx.moveTo(coords[0].x, coords[0].y);
	for ( i = 1; i < coords.length; i++ )
	{
  		if (coords[i].y == 'X' && coords[i].x == 'X')
		{
			ctx.closePath();
			ctx.fill();
			ctx.moveTo(0, 0);
			ctx.beginPath();
			if (typeof(coords[i+1]) != 'undefined')
			{
				ctx.moveTo( (coords[i+1].x), (coords[i+1].y) );
			}
		}
		else
		{
			ctx.lineTo((coords[i].x), (coords[i].y));
		}
	}
	ctx.closePath();
	ctx.fill();
}
</code>
</pre>

Now everything works perfect, unless I call draw_coords() in my draw() function - the problem is it draws and animates only the first shape, and skips the rest of them. When I skip this call it works OK.
I use the [X,X] coords to reset drawing, and start the next shape.
Can someone tell me what is wrong with that? and why when I draw that coords it only draws them on first element?